;; .emacs
(require 'package)
(add-to-list 'package-archives '("org-plus-contrib" . "https://orgmode.org/elpa/")) 
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/")) 

;;Evil mode
(require 'evil) 
(evil-mode t)

;;Org mode
(setq org-todo-keywords
        '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

;; Set languages
(org-babel-do-load-languages
  'org-babel-load-languages
  '((python . t) 
    (sagemath .t)
    )
)


;;Set python 3
(setq org-babel-python-command "python3")
;; Do not ask for code block evaluation permission
(setq org-confirm-babel-evaluate nil)
; Get easy expansions back
(use-package org-tempo) 



;;Evil org.
(require 'evil-org)
(add-hook 'org-mode-hook 'evil-org-mode)
(evil-org-set-key-theme '(textobjects insert navigation additional shift todo heading calendar))
(require 'evil-org-agenda)
(evil-org-agenda-set-keys)

;;Use guile
(setq geiser-active-implementations '(guile))
;;Autosaves in one place.
(setq backup-directory-alist `(("." . "~/.emacs.d/.saves")))



(setq custom-file "~/.emacs.d/custom.el") 
(load custom-file)
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  '(diff-switches "-u")
  '(package-selected-packages
     '(evil-org org-evil sclang-extensions cider flycheck arduino-mode org-plus-contrib evil)))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  )
