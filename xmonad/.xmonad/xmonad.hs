import XMonad
import XMonad.Config.Desktop
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Layout.IndependentScreens
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.DynamicLog

myWorkspaces = withScreens 3 ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myKeys =
         [
         -- workspaces are distinct by screen
          ((m .|. mod4Mask, k), windows $ onCurrentScreen f i)
               | (i, k) <- zip (workspaces' conf) [xK_1 .. xK_9]
               , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
         ]
         ++
         [
         -- swap screen order, [2,1,0] is screen order.
         ((m .|. mod4Mask, key), screenWorkspace sc >>= flip whenJust (windows . f))
              | (key, sc) <- zip [xK_w, xK_e, xK_r] [0,1,2]
              , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
         ]
conf = desktopConfig {
    workspaces = myWorkspaces
    , modMask = mod4Mask
    , terminal = "konsole"
    , borderWidth = 1
    , focusedBorderColor = "#00FF00"
    , normalBorderColor = "#000000"

} `additionalKeys` myKeys


main = xmonad conf
