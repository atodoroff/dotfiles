syntax on

set relativenumber
set hlsearch
set linebreak 
set showbreak=+
"set spell

set autoindent
set expandtab 
set tabstop=4 
set shiftwidth=4

set clipboard=unnamedplus

autocmd BufRead,BufNewFile *.sage,*.pyx,*.spyx set filetype=python
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0

"remove highlight search
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>


